package pl.puzzle;

import pl.puzzle.fragments.ImageFrag;
import pl.puzzle.fragments.MenuFragment;
import pl.puzzle.fragments.PuzzleFragment;
import android.os.Bundle;
import android.support.v4.app.FragmentActivity;

/**
 * Main activity of the puzzle game. Made with 3 different fragments.
 * This {@link Game} is <code>FragmentActivity</code> and implements two interfaces:
 * {@link MenuFragment#OnButtonClickListener} and
 * {@link ImageFrag#OnImageChoiceListener} to handle choices user can make
 * before he start the game.
 */
public class Game extends FragmentActivity implements 
	MenuFragment.OnButtonClickListener, ImageFrag.OnImageChoiceListener{

	public static final int START = 0;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_game);

		if (savedInstanceState == null) {
			getSupportFragmentManager().beginTransaction()
					.add(R.id.container, new MenuFragment()).commit();
		}
	}

	/**
	 * Handles callback from {@link MenuFragment}
	 * Starts new {@link ImageFrag} when {@link #START} was send.
	 */
	@Override
	public void onButtonClick(int button) {
		switch (button) {
		case START:
			ImageFrag imageFrag = new ImageFrag();
			getSupportFragmentManager().beginTransaction()
				.setCustomAnimations(R.anim.zoom_in, R.anim.zoom_out)
				.replace(R.id.container,imageFrag)
				.addToBackStack(null)
				.commit();
			break;
		default:
			break;
		}
	}

	/**
	 * Handles callback from {@link ImageFrag}.
	 * Starts {@link PuzzleFragment} with sample image.
	 */
	@Override
	public void onImageClick(int image) {	
		Bundle bundle = new Bundle();
		PuzzleFragment puzzleFrag = new PuzzleFragment();
		
		bundle.putInt("res", image); // Put sample image ID.
		puzzleFrag.setArguments(bundle);
		
		getSupportFragmentManager().beginTransaction()
			.setCustomAnimations(R.anim.zoom_in, R.anim.zoom_out)
			.replace(R.id.container,puzzleFrag)
			.addToBackStack(null)
			.commit();
	}

	/**
	 * Handles callback from {@link ImageFrag}.
	 * Starts {@link PuzzleFragment} with downloaded image.
	 */
	@Override
	public void onDownloadedClick(String ext) {		
		Bundle bundle = new Bundle();
		
		PuzzleFragment puzzleFrag = new PuzzleFragment();
		
		bundle.putString("ext", ext); // Put downloaded file extension
		puzzleFrag.setArguments(bundle);
		
		getSupportFragmentManager().beginTransaction()
			.setCustomAnimations(R.anim.zoom_in, R.anim.zoom_out)
			.replace(R.id.container,puzzleFrag)
			.addToBackStack(null)
			.commit();
	}

}
