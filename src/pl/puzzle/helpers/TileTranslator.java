package pl.puzzle.helpers;

import android.widget.LinearLayout;

/**
 * Provides functionality to get <code>ImageView</code> ID based on tile position and vice versa.
 */
public class TileTranslator {
	
	LinearLayout image_container;
	
	public TileTranslator() {
	}
	
	public TileTranslator(LinearLayout image_container) {
		this.image_container = image_container;
	}

	/**
	 * Sets <code>LinearLayout</code> that is container with all puzzle pieces in {@link #image_container}
	 * @param image_container layout to handle
	 */
	public void setImageContainer(LinearLayout image_container) {
		this.image_container = image_container;
		
	}
	
	/**
	 * Returns ID of <code>ImageView</code> that is on given position
	 * @param tile_nr tile position
	 */
	public int getIdByTile(int tile_nr) {
		if (tile_nr<16) {
			int group = tile_nr/ScreenHelper.NR_ROWS;
			LinearLayout image_row = (LinearLayout)image_container.getChildAt(group);
			return image_row.getChildAt(tile_nr%4).getId();
		} else {
			return -1;
		}
	}
	
	/**
	 * Returns position of tile that has given ID
	 * @param id
	 * @return
	 */
	public int getTileById(int id) {
		for (int i = 0; i < ScreenHelper.NR_ROWS; i++) {
			LinearLayout image_row = (LinearLayout)image_container.getChildAt(i);
			if (image_row.findViewById(id) != null) {
				for (int j = 0; j < ScreenHelper.NR_COLS; j++) {
					if (image_row.getChildAt(j).getId()==id) 
						return i*4+j;
				}
			}
		}
	return -1;
	}
}
