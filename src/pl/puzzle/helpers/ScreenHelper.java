package pl.puzzle.helpers;

import java.util.ArrayList;

import pl.puzzle.R;
import pl.puzzle.fragments.PuzzleFragment;
import android.graphics.drawable.Drawable;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.ImageView;
import android.widget.LinearLayout;

public class ScreenHelper implements OnClickListener {
	
	private PuzzleFragment puzzFrag;
	private TileTranslator translator;
	private BitmapHelper bitmapHelper;

	public static final int NR_ROWS = 4;
	public static final int NR_COLS = 4;
	
	private int ready_tile;
	
	private LinearLayout image_container;
	
	/**
	 * {@link ScreenHelper} constructor.
	 * Creates new {@link TileTranslator} to receives <code>ImageView</code>'s ID's by tile position
	 * 
	 * @param puzzFrag fragment which contains puzzle board {@link ScreenHelper} has to handle
	 * @param bitmapHelper class that handles all bitmap operations 
	 */
	public ScreenHelper(PuzzleFragment puzzFrag, BitmapHelper bitmapHelper) {
		this.bitmapHelper = bitmapHelper;
		this.puzzFrag = puzzFrag;
		
		this.translator = new TileTranslator();
		
		this.ready_tile = -1;
	}
	
	/**
	 * Remembers <code>LinearLayout</code> that contains all puzzle pieces in {@link #image_container}
	 * Sets {@link TileTranslator} to handle this layout. 
	 * @param image_container layout that contains all puzzles
	 */
	public void setImageContainer(LinearLayout image_container) {
		this.image_container = image_container;
		
		translator.setImageContainer(image_container);
		
		setImageListeners();
	}
	
	/**
	 * Sets all puzzle pieces in {@link ScreenHelper#image_container} to have the same <code>OnClickListener</code>.
	 * First we get every row, which is another <code>LinearLayout</code>,
	 * and then set listener on every image in it representing one tile.
	 */
	private void setImageListeners() {
		for (int i = 0; i < NR_ROWS; i++) {
			LinearLayout image_row = (LinearLayout)image_container.getChildAt(i);
			for (int j = 0; j < NR_COLS; j++) {
				image_row.getChildAt(j).setOnClickListener(this);
			}
		}
	}

	/**
	 * Called when one of puzzle pieces was clicked.
	 * You must first call {@link #setImageListeners()} to set listener on them.
	 * 
	 * When tile is clicked it 
	 * checks if two tiles should swap and call {@link PuzzleFragment#changeTiles(int, int)}
	 * or this is the first one selected and should save it in {@link ScreenHelper#ready_tile}
	 */
	@Override
	public void onClick(View v) {
		int clicked_tile = translator.getTileById(v.getId());
		
		Animation fade_in = AnimationUtils.loadAnimation(puzzFrag.getActivity(), R.anim.fade_in_tail);
		Animation fade_out = AnimationUtils.loadAnimation(puzzFrag.getActivity(), R.anim.fade_out_tail);
		
		if (ready_tile<0) { // User clicked first tail
			v.startAnimation(fade_out);
			ready_tile = clicked_tile;
			
		} else if (ready_tile==clicked_tile) { // User clicked the same tail twice. 
			v.startAnimation(fade_in);
			ready_tile = -1;
			
		} else { // User clicked second tile, call controller that user wants to make change
			v.startAnimation(fade_out);
			puzzFrag.changeTiles(ready_tile, clicked_tile);			
			ready_tile = -1;
		}
		
	}
	
	/**
	 * Swaps two to puzzle pieces on the screen.
	 * It gets correct <code>ImageViews</code> ID's by {@link TileTranslator#getIdByTile(int)}
	 * @param tile_1 first tile position
	 * @param tile_2 second tile position
	 */
	public void swapTiles(int tile_1, int tile_2) {
		
		Animation fade_in = AnimationUtils.loadAnimation(puzzFrag.getActivity(), R.anim.fade_in_tail);
		
		Drawable tmp;
		ImageView swap1 = (ImageView)image_container.
				findViewById(translator.getIdByTile(tile_1));
		tmp = swap1.getDrawable();
		ImageView swap2 = (ImageView)image_container.
				findViewById(translator.getIdByTile(tile_2));
		swap1.setImageDrawable(swap2.getDrawable());
		swap2.setImageDrawable(tmp);
		
		swap1.startAnimation(fade_in);
		swap2.startAnimation(fade_in);
	}

	/**
	 * Change all tiles position into new order.
	 * It gets correct <code>ImageViews</code> ID's by {@link TileTranslator#getIdByTile(int)}
	 * It gets correct tile <code>Bitmap</code> image by {@link BitmapHelper#getTileImage(int)}
	 * @param tile_order new tiles order
	 */
	public void setTilesOrder(ArrayList<Integer> tile_order) {
		
		for (int i = 0; i < NR_ROWS*NR_COLS; i++) {
			ImageView im = (ImageView)image_container
					.findViewById(translator.getIdByTile(i));
			im.setImageBitmap(bitmapHelper.getTileImage(tile_order.get(i)));
		}

	}
	
	/**
	 * Makes {@link #image_container} invisible
	 */
	public void disableBoard() {
		image_container.setVisibility(View.GONE);
	}
	
	/**
	 * Makes {@link #image_container} visible
	 */
	public void enableBoard() {
		image_container.setVisibility(View.VISIBLE);
	}
	
	public LinearLayout getImageContainer() {
		return image_container;
	}
	
}
