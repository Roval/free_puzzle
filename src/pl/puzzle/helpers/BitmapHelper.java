package pl.puzzle.helpers;

import pl.puzzle.fragments.PuzzleFragment;
import android.content.res.Resources;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;

/**
 * Helper class used by {@link PuzzleFragment} and {@link ScreenHelper} to provide 
 * correct images in puzzle pieces.
 */
public class BitmapHelper {
	
	private int TILE_X;
	private int TILE_Y;
	
	Bitmap image;
	
	/**
	 * {@link BitmapHelper} constructor when we need to use sample image bitmap.
	 * @param resources application resources
	 * @param image_id image ID's
	 */
	public BitmapHelper(Resources resources, int image_id) {
		
	    image =  BitmapFactory.decodeResource(resources, image_id);		
	    this.TILE_X = image.getWidth()/4;
		this.TILE_Y = image.getHeight()/4;
		
	}

	/**
	 * {@link BitmapHelper} constructor when we need to use downloaded image.
	 * 
	 * It also makes sure that image isn't to big
	 * by using {@link #findInSampleSize(android.graphics.BitmapFactory.Options, int, int)}.
	 * It calculates how much image should be resized before it is loaded into memory.
	 * 
	 * @param path path to downloaded image
	 */
	public BitmapHelper(String path) {
		
	    final BitmapFactory.Options options = new BitmapFactory.Options();
	    options.inJustDecodeBounds = true;
	    BitmapFactory.decodeFile(path, options);

	    // Calculate inSampleSize
	    options.inSampleSize = findInSampleSize(options, 1000, 1000);

	    // Decode bitmap with inSampleSize set
	    options.inJustDecodeBounds = false;
	    image = BitmapFactory.decodeFile(path, options);		
	    this.TILE_X = image.getWidth()/4;
		this.TILE_Y = image.getHeight()/4;
	}
	
	/**
	 * Calculates and returns the largest inSampleSize value that is a power of 2.
	 * @param options BitMap options
	 * @param reqWidth image can't have width bigger then this value
	 * @param reqHeight image can't have height bigger then this value
	 */
	private static int findInSampleSize(
			BitmapFactory.Options options, int reqWidth, int reqHeight) {

	    final int height = options.outHeight;
	    final int width = options.outWidth;
	    int inSampleSize = 1;
	
	    if (height > reqHeight || width > reqWidth) {
	
	        final int halfHeight = height / 2;
	        final int halfWidth = width / 2;
	
	        // Calculate the largest inSampleSize value that is a power of 2 and keeps both
	        // height and width larger than the requested height and width.
	        while ((halfHeight / inSampleSize) > reqHeight
	                && (halfWidth / inSampleSize) > reqWidth) {
	            inSampleSize *= 2;
	        }
	    }
	    return inSampleSize;
	}

	/**
	 * Returns bitmap image for tile on given position.
	 * @param tile_nr tile position
	 */
	public Bitmap getTileImage(int tile_nr) {
		int row = tile_nr / 4;
		int col = tile_nr % 4;
		
		return Bitmap.createBitmap(image, TILE_X*col, TILE_Y*row, TILE_X, TILE_Y);
	}
	
	public Bitmap getImage() {
		return image;
	}
}
