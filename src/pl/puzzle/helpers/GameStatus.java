package pl.puzzle.helpers;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Timer;
import java.util.TimerTask;

/**
 * Provides all data about current game state.
 */
public class GameStatus {
	
	private ArrayList<Integer> GAME_STATUS;
	private Timer TIMER;
	private float seconds;
	
	/**
	 * GameStatus constructor.
	 * Preparing new rendom tiles order.
	 */
	public GameStatus() {
		this.GAME_STATUS = new ArrayList<>();
		this.seconds = 0.0f;
		
		for (int i = 0; i < 16; i++) {
			this.GAME_STATUS.add(i);
		}
		
		Collections.shuffle(GAME_STATUS);
	}
	
	/**
	 * Returns current tiles order
	 */
	public ArrayList<Integer> getGameStatus(){
		return GAME_STATUS;
	}
	
	/**
	 * Returns correct tiles order 
	 */
	public ArrayList<Integer> getCleanStatus(){
		ArrayList<Integer> tmp = new ArrayList<>();
		for (int i = 0; i < 16; i++) {
			tmp.add(i);
		}
		return tmp;
	}
	
	/**
	 * Creates a new Timer and starts counting time. 
	 * Timer is running {@link SecondsCounter} task every 0.1 second.
	 * Called when games starts.
	 */
	public void startTimer() {
		TIMER = new Timer();
		TIMER.scheduleAtFixedRate(new SecondsCounter(), 0, 100);
	}
	
	/**
	 * Stops Timer if exists and returns time between {@link #startTimer()} and {@link #stopTimer()}.
	 * Returns 0 if {@link #startTimer()} wasn't called.
	 */
	public float stopTimer() {
		if (TIMER != null) {
			TIMER.cancel();
			TIMER.purge();
			TIMER = null;
			return seconds;
		}
		return 0.0f;
	}
	
	/**
	 * Updates game status. Swaps tiles with {@link #swap_tiles(int, int)} to represents new order.
	 * Returns {@link #isComplete()} result to decide whether the game is complete.
	 * @param tile_1 first tile position
	 * @param tile_2 second tile possition
	 */
	public boolean updateGameStatus(int tile_1, int tile_2) {
		swap_tiles(tile_1, tile_2);
		return isComplete();
	}

	/**
	 * Swaps tile on position tile_1 with tile on position tile_2.
	 * @param tile_1 first tile position
	 * @param tile_2 second tile position
	 */
	private void swap_tiles(int tile_1, int tile_2) {
		Integer tmp = GAME_STATUS.get(tile_1);
		GAME_STATUS.set(tile_1, GAME_STATUS.get(tile_2));
		GAME_STATUS.set(tile_2, tmp);
	}
	
	/**
	 * Returns true if tiles are in the correct order.
	 * Returns false if tiles are not in the correct order.
	 */
	private boolean isComplete() {
		int i = 0;
		for (int tile_order: GAME_STATUS) {
			if (tile_order==i) i++;
			else return false;
		}
		return true;
	}
	
	/**
	 * Reset all GameStatus fields and shuffle tiles order.
	 */
	public void reset() {
		Collections.shuffle(GAME_STATUS);
		stopTimer();
		seconds = 0.0f;
	}
	
	/**
	 * Simple TimerTask class to count time every 0.1 second. 
	 * Used by {@link #startTimer}
	 */
	private class SecondsCounter extends TimerTask {
	    @Override public void run() {
	    	seconds+=0.1f; 
	    }
	}

}
