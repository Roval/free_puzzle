package pl.puzzle.fragments;

import java.text.NumberFormat;

import pl.puzzle.R;
import pl.puzzle.helpers.BitmapHelper;
import pl.puzzle.helpers.GameStatus;
import pl.puzzle.helpers.ScreenHelper;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.MotionEvent;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.View.OnTouchListener;
import android.view.ViewGroup;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

/**
 * Main gameplay fragment which uses {@link GameStatus} to be his model 
 * and {@link ScreenHelper} to be his view.
 * 
 * layout: fragment_puzzle
 */
public class PuzzleFragment extends Fragment {
	
	private ScreenHelper screen;
	private GameStatus status;
	private BitmapHelper bitmapHelper;
	
	/**
	 * When user complete the image we can't allow him to click anything but return.
	 * <code>true</code> - user can click another options
	 * <code>false</code> - user can't click another options
	 */
	private boolean enable_clicks;
	
	
	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container,
			Bundle savedInstanceState) {
		
		View view = inflater.inflate(R.layout.fragment_puzzle, container,
					false);

		return view;
	}
	
	/**
	 * Creates new {@link GameStatus} to handle all model operations.
	 * Creates new {@link ScreenHelper} to handle all view operations
	 * and use {@link ScreenHelper#setImageContainer(LinearLayout)} to set puzzle board in it.
	 * Sets all necessary listeners with {@link #setRedoImage()} and {@link #setSolveImage()}.
	 * 
	 * Makes {@link #status} start counting time by {@link GameStatus#startTimer()}
	 */
	@Override
	public void onViewCreated(View view, Bundle savedInstanceState) {
		super.onViewCreated(view, savedInstanceState);
		
		status = new GameStatus();

		Bundle args = this.getArguments();
		int res = args.getInt("res",0);
		String ext = args.getString("ext");
		
		if (ext==null) {
			bitmapHelper = new BitmapHelper(getResources(), res);
		} else {
			bitmapHelper = new BitmapHelper(getActivity().getFilesDir()+"/image."+ext);
		}

		screen = new ScreenHelper(this, bitmapHelper);		
		screen.setImageContainer(
				(LinearLayout)getView().findViewById(R.id.image_container));
		screen.setTilesOrder((status.getGameStatus()));
		
		setRedoImage();
		setSolveImage();
		
		enable_clicks = true;
		
		status.startTimer();
	}
	
	/**
	 * Handles changes that user made.
	 * Makes {@link #screen} swap selected puzzle pieces by {@link ScreenHelper#swapTiles(int, int)}
	 * 
	 * Updates game status by {@link GameStatus#updateGameStatus(int, int)}
	 * and this method also returns <code>true</code> or <code>false</code>
	 * if game should end.
	 * 
	 * If game should end it disables puzzle board, stops {@link #status} timer by
	 * {@link GameStatus#stopTimer()} and gets result from it.
	 * 
	 * @param tile_1 first tile position
	 * @param tile_2 second tile position
	 */
	public void changeTiles(int tile_1, int tile_2) {

		screen.swapTiles(tile_1, tile_2);
		
		if (status.updateGameStatus(tile_1, tile_2)) {
			
			Float seconds_result = status.stopTimer();
			Log.i("DEBUG",seconds_result.toString());
			enable_clicks = false;

			Animation fade_in = AnimationUtils.loadAnimation(getActivity(), R.anim.fade_in);
			Animation fade_out = AnimationUtils.loadAnimation(getActivity(), R.anim.fade_out);
			
			screen.getImageContainer().startAnimation(fade_out);
			screen.disableBoard();
			
			TextView timer_view = (TextView)getView().findViewById(R.id.timer);
			timer_view.setText("Time: " + roundFloat(seconds_result));
			
			getView().findViewById(R.id.end_game)
				.setVisibility(View.VISIBLE);
			getView().findViewById(R.id.end_game)
				.startAnimation(fade_in);
		}
	}
	
	/**
	 * Returns string representation of float number.
	 * @param number float number to round
	 */
	private String roundFloat(float number) {
		NumberFormat formatter = NumberFormat.getNumberInstance();
		formatter.setMinimumFractionDigits(1);
		return formatter.format(number);
	}
	
	/**
	 * Sets <code>ImageView</code> to act like button.
	 * When user clicks this image game should restart.
	 * 
	 * First it restart all {@link #status} fields by {@link GameStatus#reset()}
	 * and set {@link #screen} to show new tiles order by
	 * {@link ScreenHelper#setTilesOrder(java.util.ArrayList)}
	 * 
	 * If {@link #enable_clicks} value is <code>false</code> it means that user ended
	 * last game and result view should be set be invisible.
	 */
	private void setRedoImage() {
		ImageView redo_button = (ImageView)getView().findViewById(R.id.redo_button);
		redo_button.setOnClickListener(new OnClickListener() {
			
			@Override
			public void onClick(View v) {			
				status.reset();
				screen.setTilesOrder(status.getGameStatus());
				
				if (!enable_clicks) {
					
					Animation fade_in = AnimationUtils.loadAnimation(getActivity(), R.anim.fade_in);
					Animation fade_out = AnimationUtils.loadAnimation(getActivity(), R.anim.fade_out);
					getView().findViewById(R.id.end_game).startAnimation(fade_out);
					getView().findViewById(R.id.end_game).setVisibility(View.GONE);
					screen.enableBoard();
					screen.getImageContainer().startAnimation(fade_in);
					enable_clicks = true;
				}
				
				status.startTimer();
			}
		});
	}
	
	/**
	 * Sets tiles order in {@link #screen} to represent complete image.
	 * Correct order is received by {@link GameStatus#getCleanStatus()}
	 * Uses <code>onTouchListener</code> to know if finger was released.
	 */
	private void setSolveImage() {
		ImageView solve_button = (ImageView)getView().findViewById(R.id.solve_button);
		
		solve_button.setOnTouchListener(new OnTouchListener() {
			@Override
			public boolean onTouch(View v, MotionEvent event) {
				int action = event.getAction();
				if (enable_clicks) {
					if (action == MotionEvent.ACTION_DOWN) {
						screen.setTilesOrder(status.getCleanStatus());
						return true;
					} else if (action == MotionEvent.ACTION_UP) {
						screen.setTilesOrder(status.getGameStatus());
						return true;
					}
				}
		        return false;
			}
		});
	}
	

}
