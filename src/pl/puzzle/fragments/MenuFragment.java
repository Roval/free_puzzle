package pl.puzzle.fragments;

import pl.puzzle.Game;
import pl.puzzle.R;
import android.app.Activity;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.ImageView;

/**
 * Simple menu fragment. 
 * Layout: fragment_menu
 */
public class MenuFragment extends Fragment implements OnClickListener {
	
	private OnButtonClickListener mCallback;

    /**
     * {@link Game} must implements this interface.
     */
    public interface OnButtonClickListener {
        public void onButtonClick(int button);
    }
	
	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container,
			Bundle savedInstanceState) {
		View view = inflater.inflate(R.layout.fragment_menu, container,
				false);
		return view;
	}
	
	/**
	 * Sets <code>ImageView</code> to act like button by <code>onClickListener</code>.
	 */
	@Override
	public void onViewCreated(View view, Bundle savedInstanceState) {
		super.onViewCreated(view, savedInstanceState);
		
		ImageView start_btn = (ImageView)getView().findViewById(R.id.start_button);
		
		start_btn.setOnClickListener(this);
		
	}
	
	/**
	 * Makes sure that the container activity has implemented
	 * the callback interface {@link OnButtonClickListener}. If not, it throws an exception.
	 */
	@Override
	public void onAttach(Activity activity) {
		super.onAttach(activity);
		
		// This
        try {
            mCallback = (OnButtonClickListener) activity;
        } catch (ClassCastException e) {
            throw new ClassCastException(activity.toString()
                    + " must implement OnButtonClickListener");
        }
	}

	/**
	 * Handles buttons clicks.
	 * Makes callback to main activity 
	 * when clicked by {@link OnButtonClickListener#onButtonClick(int)}
	 */
	@Override
	public void onClick(View v) {
		switch (v.getId()) {
		case R.id.start_button:
			mCallback.onButtonClick(Game.START);
			break;
		default:
			break;
		}
		
	}
	
}
