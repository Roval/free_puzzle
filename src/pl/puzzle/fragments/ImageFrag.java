package pl.puzzle.fragments;

import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.net.HttpURLConnection;
import java.net.URL;

import pl.puzzle.Game;
import pl.puzzle.R;
import android.app.Activity;
import android.content.Context;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.view.inputmethod.InputMethodManager;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

/**
 * Fragment where user can choose whether he wants sample image 
 * or download image from web.
 * 
 * layout: fragment_image
 */
public class ImageFrag extends Fragment implements OnClickListener{
	
	private OnImageChoiceListener mCallback;
	
	private boolean downloading = false;
	
	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container,
			Bundle savedInstanceState) {
		View view = inflater.inflate(R.layout.fragment_image, container,
				false);
		return view;
	}
	
	/**
	 * Sets all necessary listeners after view was created.
	 */
	@Override
	public void onViewCreated(View view, Bundle savedInstanceState) {
		setImageListeners();
		setDownloadListener();
		super.onViewCreated(view, savedInstanceState);
	}
	
    /**
     * {@link Game} must implements this interface.
     * {@link OnImageChoiceListener#onImageClick(int)} called when 
     * user wants image from <code>/res/drawables</code>
     * {@link OnImageChoiceListener#onDownloadedClick(String)} called when
     * user wants download and use image from Internet
     */
    public interface OnImageChoiceListener {
        public void onImageClick(int image);
        public void onDownloadedClick(String ext);
    }

	/**
	 * Makes makes sure that the container activity has implemented
	 * the callback interface {@link OnImageChoiceListener}. If not, it throws an exception.
	 */
    @Override
	public void onAttach(Activity activity) {
		super.onAttach(activity);
        try {
            mCallback = (OnImageChoiceListener) activity;
        } catch (ClassCastException e) {
            throw new ClassCastException(activity.toString()
                    + " must implement OnImageChoiceListener");
        }
	}
    
    /**
     * Sets download <code>ImageView</code> to act like button.
     * When user want's to download image from web first it makes sure to hide keyboard.
     * 
     * User can download file only if {@link #downloading} value is <code>false</code>,
     * which means user isn't currently downloading another file.
     * 
     * This method gets URL address from <code>EditView</code>
     * and tries to download. Then hides sample images and shows simple circle progress bar.
     * 
     * If user can't download file quick <code>Toast</code> is visible.
     */
    private void setDownloadListener() {
    	final ImageView download_button = (ImageView)getView().findViewById(R.id.download_button);
    	download_button.setOnClickListener(new OnClickListener() {
			
			@Override
			public void onClick(View v) {
				
				InputMethodManager imm = (InputMethodManager)getActivity().getSystemService(
					      Context.INPUT_METHOD_SERVICE);
				imm.hideSoftInputFromWindow(download_button.getWindowToken(), 0);
				
				if (!downloading) {
					TextView url = (TextView)getView().findViewById(R.id.urladdress);
					if (downloadImage(url.getText().toString())) {
						getView().findViewById(R.id.test_images).setVisibility(View.GONE);
						getView().findViewById(R.id.download_bar).setVisibility(View.VISIBLE);
					} else {
						Toast.makeText(getActivity(), "Wrong file extension", Toast.LENGTH_SHORT).show();
					}
				}
			}
		});
    }
    
    /**
     * Sets <code>OnClickListener</code> to all <code>ImageViews</code>
     * with sample image.
     */
    private void setImageListeners() {
    	final int IM_VIEWS_ID[] = { //All hardcoded ImageView's ID's
    			R.id.test_image_1,
    			R.id.test_image_2,
    			R.id.test_image_3,
    			R.id.test_image_4
    	};
    	
		for (int i = 0; i < IM_VIEWS_ID.length; i++) {
			getView().findViewById(IM_VIEWS_ID[i]).setOnClickListener(this);
		}
    }
    
    /**
     * Handles all clicks and call {@link Game} activity to 
     * start game with clicked image.
     */
	@Override
	public void onClick(View v) {
		switch (v.getId()) {
		case R.id.test_image_1:
			mCallback.onImageClick(R.drawable.test1);
			break;
		case R.id.test_image_2:
			mCallback.onImageClick(R.drawable.test2);
			break;
		case R.id.test_image_3:
			mCallback.onImageClick(R.drawable.test3);
			break;
		case R.id.test_image_4:
			mCallback.onImageClick(R.drawable.test4);
			break;
		default:
			break;
		}
		
	}
    
    /**
     * Returns <code>true</code> if downloading task was started correctly.
     * Starts new {@link DownloadTask} - <code>AsyncTask</code> to download image from URL.
     * Uses {@link #checkExt(String)} to make sure its image.
     */
    private boolean downloadImage(String url) {
		final DownloadTask downloadTask = new DownloadTask();
		final String ext;
		if ((ext = checkExt(url))!= null){
			downloadTask.execute(url,ext);
			return true;
		}
    	return false;
    }
    
    /**
     * Show sample images again, because {@link DownloadTask} couldn't download file.
     * Show simple <code>Toast</code> to inform user.
     */
    private void downloadError() {
		getView().findViewById(R.id.test_images).setVisibility(View.VISIBLE);
		getView().findViewById(R.id.download_bar).setVisibility(View.GONE);
		Toast.makeText(getActivity(), "Cannot download file", Toast.LENGTH_SHORT).show();
    }
    
    /**
     * Returns file extension based on URL if it is image extension.
     * Returns <code>null</code> if it's not image.
     * @param url address of image
     */
    private String checkExt(String url) {
    	String ext = url.substring(url.lastIndexOf('.') + 1, url.length());
    	if (ext.equals("jpg") || ext.equals("png") || ext.equals("bnp")) 
    		return ext;
    	else 
    		return null;
    }
    
    private class DownloadTask extends AsyncTask<String, Integer, String> {
    	
    	private String ext;

        public DownloadTask() {
        }
        
        /**
         * Sets {@link ImageFrag#downloading} value to <code>false</code>.
         * It prevents creating new {@link DownloadTask}.
         */
        @Override
        protected void onPreExecute() {
        	downloading = true;
        	super.onPreExecute();
        }

        /**
         * Downloads image from URL. 
         * Image saves itself in private application directory.
         */
        @Override
        protected String doInBackground(String... parms) {

            InputStream input = null;
            OutputStream output = null;
            HttpURLConnection connection = null;
            
            try {
                URL url = new URL(parms[0]);
                ext = parms[1];
                
                connection = (HttpURLConnection) url.openConnection();
                connection.connect();
                
                if (connection.getResponseCode() != HttpURLConnection.HTTP_OK) {
                    return connection.getResponseCode() + " " + connection.getResponseMessage();
                }

                input = connection.getInputStream();
                output = new FileOutputStream(getActivity().getFilesDir()+"/image."+ext);

                byte data[] = new byte[4096];
                int count;
                
                while ((count = input.read(data)) != -1) {
                    output.write(data, 0, count);
                }
            } catch (Exception e) {
            	return e.toString();
            } finally {
                try {
                    if (output != null)
                        output.close();
                    if (input != null)
                        input.close();
                } catch (IOException ignored) { }

                if (connection != null)
                    connection.disconnect();
            }
            return null;
        }
        
        /**
         * Calls {@link Game} to start game with downloaded image.
         * If downloading couldn't be completed report it with {@link ImageFrag#downloadError()}
         */
        @Override
        protected void onPostExecute(String result) {
        	downloading = false;
            if (result != null)
            	ImageFrag.this.downloadError();
            else
            	mCallback.onDownloadedClick(ext);
        }
    }

       
    
}
